package t2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Semaphore;

class ThreadServidor implements Runnable{
	
	private static int contador = 1;

	private boolean livre = true;
	private Thread t;
	private int idServidor;
	private int numPedido;
	private String path;
	private Semaphore semaforo;

	private BufferedWriter buffWrite;
	
	ThreadServidor(Semaphore semaforo){
		this.semaforo = semaforo;
		this.idServidor = contador++;
		this.path = Integer.toString(idServidor) + ".txt";
	}
	
	public void escreveArquivo(String s) throws IOException {
		buffWrite = new BufferedWriter(new FileWriter(path, true));
		buffWrite.append(s); 
		buffWrite.close();
	}
	
	public void setNumPedido(int num) {
		this.numPedido = num;
	}
	
	public boolean getLivre() {
		return this.livre;
	}
	
	public int getIdServidor() {
		return idServidor;
	}

	private static boolean isPrime(int num) {
		if (num <= 1)
			return false;
		
		for (int i = 2; i <= Math.sqrt(num); i++) {
			if (num % i == 0)
				return false;
		}	
		
		return true;
    }

	public void run(){
		
		this.livre = false;
		
		/*Pedido de leitura*/
		if(this.numPedido == 1) {
		
			try {
				String texto = "Servidor "+ idServidor +"\n"+Files.readString(Path.of(path));	
				System.out.println(texto);
				
			} catch (IOException e) {
				System.out.println("Arquivo não existe ainda");
			}
			
		}
		/*Escrita no arquivo*/
		else {
			boolean isPrime = ThreadServidor.isPrime(numPedido);
			String resposta;
			
			if(isPrime) {
				resposta = "O valor "+numPedido + " é primo\n";
			}
			else {
				resposta = "O valor "+ numPedido + " não é primo\n";
			}

			try {
				
				semaforo.acquire();
				escreveArquivo(resposta);
				Servidor.mantemConsistencia(resposta, idServidor);
				
			} catch (InterruptedException | IOException e) {
			}
			finally {
				semaforo.release();
			}
			
		}
		this.livre = true;
	}
	
	public void start() {
		if( t == null) {
			t = new Thread(this);
			t.start();
		}
	}
}