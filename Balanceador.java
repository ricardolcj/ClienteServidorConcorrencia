package t2;

import java.util.Queue;

public class Balanceador implements Runnable{

	private Queue <ThreadServidor>filaServidores;
	private Queue <Integer>filaNumeros;
	private Thread t;
	
	
	Balanceador(Queue <ThreadServidor>filaServidores, Queue <Integer>filaNumeros){
		this.filaServidores = filaServidores;
		this.filaNumeros = filaNumeros;
	}
	@Override
	public void run() {
		while(true) {
			while(!filaNumeros.isEmpty()) {
				
				ThreadServidor s = filaServidores.remove();
			    while(!s.getLivre()) {
			    	filaServidores.add(s);
			    	s = filaServidores.remove();
			    }
			    filaServidores.add(s);

				int primeiroFila = filaNumeros.remove();
				s.setNumPedido(primeiroFila);
				s.run();
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {

			}
		}
		
	}
	public void start() {
		if( t == null) {
			t = new Thread(this);
			t.start();
		}
	}
}