package t2;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ThreadLocalRandom;

class ThreadCliente implements Runnable {
	
	private static int contador = 1;
	  
	private Socket cliente;
	private Thread t;
	private int idThread;
  
	ThreadCliente(){
		this.idThread = contador++;
	}
  
	private Socket initCliente(){
	    try {
	    	cliente = new Socket("127.0.0.1", 3061);
	        
	    } catch (IOException ex) {
	        
	    }
	    return cliente;
	}

	public void run() {
		while(true) {
			try {
				/*Inicializa a conexão com o servidor*/
				this.cliente = initCliente();
				
				/*Requisição de leitura ou escrita. 20% leitura*/
				int tipo = ThreadLocalRandom.current().nextInt(1, 5);

				/*Gera um número aleatório entre 2 e 1000000*/
				int num = ThreadLocalRandom.current().nextInt(2, 1000000);
				
				if(tipo == 1)
					num = tipo;

				/*Abre entrada e saída de dados*/
				PrintWriter out = new PrintWriter(cliente.getOutputStream(), true);

				/*Envia o número para o servidor*/
				out.println(num);

				/*Coloca a Thread pra dormir*/
			    Thread.sleep(ThreadLocalRandom.current().nextInt(5000, 10000));
			}	
			catch(Exception e) {
				
			}
		}
	}
	
	public void start() {
		System.out.println("Criando cliente C" + idThread);
		
		if( t == null) {
			t = new Thread(this);
			t.start();
		}
	}
}