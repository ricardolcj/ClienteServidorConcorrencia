package t2;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException, InterruptedException {

		new Servidor().start();
		new Cliente().start();
	}
}
