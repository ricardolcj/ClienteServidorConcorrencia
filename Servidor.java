package t2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public class Servidor implements Runnable{
	
	private Thread t;
	private static Semaphore semaforo = new Semaphore(1, true);
	private static Queue <Integer>filaNumeros = new LinkedList<Integer>();
	private static Queue <ThreadServidor>filaServidores = new LinkedList<ThreadServidor>();
	private static LinkedList<ThreadServidor> listaServidores = new LinkedList<ThreadServidor>();
	
	
	public static void mantemConsistencia(String s, int idOrigem) throws IOException {
		for(int i = 0; i < listaServidores.size(); i++) {
			ThreadServidor servidor = listaServidores.get(i);
			if(servidor.getIdServidor()!=idOrigem)
				servidor.escreveArquivo(s);
		}	
	}
	
	public void iniciar() throws IOException {
		
		@SuppressWarnings("resource")
		/*Abre o dispatcher na porta 3061*/
		ServerSocket server = new ServerSocket(3061);
		
		int M = 3;
		
		for(int i = 0; i < M; i++) {
			ThreadServidor s = new ThreadServidor(semaforo);
			filaServidores.add(s);
			listaServidores.add(s);
		}
		new Balanceador(filaServidores, filaNumeros).start();
		
		while(true) {
			/*Espera um novo cliente*/
			Socket cliente = server.accept();
			
		    /*Abre entrada de dados*/
			BufferedReader entradaSocket = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
			
		    /*Recebe o número enviado pelo cliente*/
		    int numPedido = Integer.parseInt(entradaSocket.readLine());
		    
		    /*Coloca o número passado pelo cliente na fila*/
		    filaNumeros.add(numPedido);
		    
		}	
	}

	@Override
	public void run() {
		try {
			iniciar();
		} catch (IOException e) {

		}	
	}
	public void start() {
		if( t == null) {
			t = new Thread(this);
			t.start();
		}
	}
}